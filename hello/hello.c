/* Copyright © 2021: Bart De Vos */
#include <stdio.h>

int main (int argc, char * argv[])
{
    /* print the given argument */
    if (argc > 1) {
        printf("Hello %s\n", argv[1]);
    } else {
        /* sane defaults */
        printf("Hello World\n");
    }

    return 0;
}
