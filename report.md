|    filepath     | passed | failed | error | skipped | xfailed | xpassed | SUBTOTAL |
| --------------- | -----: | -----: | ----: | ------: | ------: | ------: | -------: |
| test_error.py   |        |        |     2 |         |         |         |        2 |
| test_failed.py  |        |      2 |       |         |         |         |        2 |
| test_pass.py    |      2 |        |       |         |         |         |        2 |
| test_skipped.py |        |        |       |       2 |         |         |        2 |
| test_xfailed.py |        |        |       |         |       2 |         |        2 |
| test_xpassed.py |        |        |       |         |         |       2 |        2 |
| TOTAL           |      2 |      2 |     2 |       2 |       2 |       2 |       12 |
